# Toptimiz3D Parallel version -- Docker container

The files in this repository show how to build and run a Toptimiz3D
Docker images/containers with graphics support. It is based on
[this](https://github.com/gklingler/docker3d)

## Requirements

- Docker

- (**Windows only**) PowerShell needs permission to execute scripts. Execute:

```
Set-ExecutionPolicy RemoteSigned
```

- (**Windows only**) X Server program [VcXsrv](https://sourceforge.net/projects/vcxsrv/)

#### If your host is running a NVIDIA driver
- Get the driver version with

```
nvidia-smi
```

- Download the Linux NVIDIA driver (same version) from
  [NVIDIA](https://www.nvidia.com/en-us/drivers/unix/linux-amd64-display-archive/). Rename
  as `NVIDIA-DRIVER.run`

**Alternative** In some Windows, NVIDIA-DRIVER.run does not work. In
that case, installing nvidia-driver-N from ubuntu repositories
work. Use N close to your Windows nvidia driver version. In this case,
remove `install nvidia driver` section from Dockerfile and uncomment
line `#RUN apt install -y nvidia-driver-455` and comment `RUN apt
install -y`

## Docker build

Put `parallel-script.sh` and `Dockerfile` (from the corresponding folder)
along with `NVIDIA-DRIVER.run` (if necessary) in the same folder. Run

```
docker build -t paratopt .
```

It takes some minutes to complete.

## Run Toptimiz3D

(**Windows only**) Execute `winrun.ps1` script.

(**Other systems**) Execute run.sh script.
