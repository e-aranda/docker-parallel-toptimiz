#!/bin/bash
set -x
cd /home/toptimiz
mkdir software
cd software
mkdir bin

# instalación de paquetes específicios para toptimiz en /opt

# HYPRE

wget https://github.com/hypre-space/hypre/archive/refs/tags/v2.22.1.zip

unzip v2.22.1.zip
cd hypre-2.22.1/src
./configure --disable-fortran
make -j
cd ../..
ln -s hypre-2.22.1 hypre
rm -f v2.22.1.zip

# METIS
wget http://glaros.dtc.umn.edu/gkhome/fetch/sw/metis/metis-5.1.0.tar.gz
tar xvfz metis-5.1.0.tar.gz
cd metis-5.1.0
make config
make
mkdir lib
cd lib
ln -s ../build/Linux-x86_64/libmetis/libmetis.a .
cd ../..
rm metis-5.1.0.tar.gz

#MFEM
wget https://github.com/mfem/mfem/archive/refs/heads/master.zip
unzip master.zip
cd mfem-master
make parallel -j MFEM_USE_METIS_5=YES METIS_DIR=@MFEM_DIR@/../metis-5.1.0
cd ..
rm master.zip


# GLVIS
wget https://bit.ly/glvis-4-0
tar xfz glvis-4-0
cd glvis-4.0
make MFEM_DIR=../mfem-master/ -j
cp glvis ../bin
cd ..
rm glvis-4-0


# hiop
# wget ...
# tar ...
# cd .../build
# cmake -DMPI_CXX_COMPILER_INCLUDE_DIRS=/usr/include/x86_64-linux-gnu/mpi -DCMAKE_INSTALL_PREFIX=/opt/software/hiop ..
# make test
# make install

#Toptimiz3D Parallel
wget https://gitlab.com/e-aranda/partoptimiz/-/archive/master/partoptimiz-master.tar.gz
tar xvfz partoptimiz-master.tar.gz 
cd partoptimiz-master
python3 install.py --cpp="-I/home/toptimiz/software/hypre/src/hypre/include/ -I/home/toptimiz/software/mfem-master/" --ld="-L/home/toptimiz/software/hypre/src/hypre/lib -L/home/toptimiz/software/mfem-master" --yes

cd ..
rm *.tar.gz
rm -fR partoptimiz-master



